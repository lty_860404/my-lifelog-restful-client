//
//  BSLAppDelegate.h
//  BSLifelogRestClient DEMO
//
//  Created by tianyin luo on 14-6-4.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
