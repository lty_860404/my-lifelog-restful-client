//
//  BSLFileViewController.h
//  BSLifelogRestClient DEMO
//
//  Created by tianyin luo on 14-6-9.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLFileViewController : UIViewController

@property (weak,nonatomic) IBOutlet UIImageView *imageView;

-(IBAction)testFileGet:(id)sender;

@end
