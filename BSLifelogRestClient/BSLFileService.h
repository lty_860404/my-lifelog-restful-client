//
//  BSLFileService.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-6-9.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLService.h"
#import "BSLFileRouteManager.h"
#import "BSLError.h"

@interface BSLFileService : BSLService

    +(instancetype) getInstance;

    -(void) getFile:(NSString*) fileKey
            success:(void (^)(NSData *data))success
            failure:(void (^)(NSError *err))failure;

@end
