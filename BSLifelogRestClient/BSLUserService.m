//
//  BSLUserService.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLUserService.h"
#import "BSLUserRouteManager.h"
#import "BSLSystemConfig.h"
#import "RKMIMETypeSerialization.h"

@interface BSLUserService()

@end

@implementation BSLUserService

    static BSLUserService *useService = nil;

    +(instancetype) getInstance{
        if(useService==nil){
            BSLRestClient *client = [[BSLRestClient alloc] initWithBaseURL:[BSLSystemConfig baseURL]];
            BSLUserRouteManager* userRouteMgr = [[BSLUserRouteManager alloc] initWithLoadDatas];
            [client loadRoutes:userRouteMgr];
            useService = [[BSLUserService alloc] init];
            [useService setRestClient:client];
        }
        return useService;
    }

    @synthesize restClient;

    //if success return BSLSuccessMsg else return BSLError
    -(void) verify:(NSString *)phone
           success:(void (^)(BSLSuccessMsg *successMsg))success
           failure:(void (^)(BSLError *err))failure;
    {
        
        BSLUser* user = [[BSLUser alloc] init];
        [user setPhone:phone];
        NSLog(@"verify user phone:%@",[user phone]);
        NSLog(@"verify start!");
        [restClient postObject:user
                      identify:@ROUTE_USER_VERIFY_ID
                          path:@ROUTE_USER_VERIFY_PATH
                    parameters:nil
                       success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                      NSLog(@"verify response:%@",[mappingResult description]);
                                      BSLSuccessMsg* msg = [mappingResult firstObject];
                                      success(msg);
                                  }
                       failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                      NSLog(@"verify ERROR:%@",[error localizedDescription]);
                                      BSLError* err = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                                      failure(err);
                                  }];
    }

    -(void) getUserInfoWithId:(long)userId
                        success:(void (^)(BSLUser *user))success
                        failure:(void (^)(BSLError *err))failure{
        BSLUser *user = [[BSLUser alloc] init];
        [user setId:userId];
        [restClient getObject:user
         relationshipIdentify:@ROUTE_USER_INFO_ID
                   parameters:nil
                      success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                 NSLog(@"user Info response:%@",[mappingResult description]);
                                                 BSLUser *user = [mappingResult firstObject];
                                                 success(user);
                                             }
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                 NSLog(@"get user Info ERROR:%@",[error localizedDescription]);
                                                 BSLError* err = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                                                 failure(err);
                                             }];
    }


    -(void) getUsersInfoSuccess:(void (^)(NSArray *usersInfoArr))success
                        failure:(void (^)(BSLError *err))failure{
        [restClient getObject:nil
                routeIdentify:@ROUTE_USERS_INFO_ID
                   parameters:nil
                      success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                 NSLog(@"users Info response:%@",[mappingResult description]);
                                                 NSArray* userInfoArr = [mappingResult array];
                                                 success(userInfoArr);
                                             }
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                 NSLog(@"users Info Error:%@",[error localizedDescription]);
                                                 BSLError *err = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                                                 failure(err);
                                             }];
    }

    -(void) addUser:(BSLUserAddParams *)user
            success:(void (^)(BSLSuccessMsg *successMsg))success
            failure:(void (^)(BSLError *err))failure
    {
        [restClient postObject:user
                      identify:@ROUTE_USER_ADD_ID
                          path:@ROUTE_USER_ADD_PATH
                    parameters:nil
                       success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                      NSLog(@"user Add response:%@",[mappingResult description]);
                                      BSLSuccessMsg* msg = [mappingResult firstObject];
                                      success(msg);
                                  }
                       failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                      NSLog(@"user Add Error:%@",[error localizedDescription]);
                                      BSLError *err = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                                      failure(err);
                                  }];
    }

    -(void) delUserWithId:(long)userId
                  success:(void (^)(BSLSuccessMsg *successMsg))success
                  failure:(void (^)(BSLError *err))failure{
        BSLUser *user = [[BSLUser alloc] init];
        [user setId:userId];
        [restClient deleteObject:user
                    relationship:@ROUTE_USER_DELETE_ID
                            path:@ROUTE_USER_DELETE_PATH
                      parameters:nil
                         success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                            NSLog(@"user Delete Response:%@",[mappingResult description]);
                                            BSLSuccessMsg* msg = [mappingResult firstObject];
                                            success(msg);
                                        }
                         failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                            NSLog(@"user Delete Error:%@",[error localizedDescription]);
                                            BSLError *err = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                                            failure(err);
                                        }];
    }

    -(void) changeAvator:(UIImage*)avatar
                filename:(NSString *)filename
                  userId:(long)userId
                 success:(void (^)(BSLData *data))success
                failure:(void (^)(BSLError *err))failure
    {
        BSLUser *user = [[BSLUser alloc] init];
        [user setId:userId];
        if(avatar==nil){
            NSLog(@"avatar is nil");
        }
        [restClient uploadImage:avatar
                           name:@"avatar"
                       filename:filename
                         object:user
                   relationShip:@ROUTE_USER_CHANGE_AVATAR_ID
                     parameters:nil
                        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                            NSLog(@"change avator result:%@",[mappingResult description]);
                            BSLData* data = [mappingResult firstObject];
                            success(data);
                        }
                        failure:^(RKObjectRequestOperation *operation, NSError *error) {
                            NSLog(@"change avator Error:%@",[error localizedDescription]);
                            BSLError *err = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                            failure(err);
                        }];
    }

-(void) updateUserInfo:(BSLUserUpdateParams *)userUpdateParam
               success:(void (^)(BSLSuccessMsg *successMsg))success
               failure:(void (^)(BSLError *err))failure
{
    [restClient setRequestSerializationMIMEType:RKMIMETypeJSON];
    [restClient postObject:userUpdateParam
      relationShipIdentify:@ROUTE_USER_UPDATE_ID
                parameters:nil
                   success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                       NSLog(@"user Update response:%@",[mappingResult description]);
                       BSLSuccessMsg* msg = [mappingResult firstObject];
                       success(msg);
                   } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                       NSLog(@"user Update Error:%@",[error localizedDescription]);
                       BSLError *err = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                       failure(err);
                   }];
}



@end
