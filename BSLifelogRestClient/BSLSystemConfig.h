//
//  BSLSystemConfig.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLSystemConfig : NSObject

+(NSURL *) baseURL;
+(void) setBaseURL:(NSURL *)baseURL;

@end
