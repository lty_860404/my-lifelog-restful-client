//
//  BSLUserRouter.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLUserRouteManager.h"
#import "RKObjectMapping.h"
#import "BSLUser.h"
#import "BSLUserAddParams.h"
#import "BSLUserUpdateParams.h"
#import "BSLSuccessMsg.h"
#import "BSLData.h"

@implementation BSLUserRouteManager

    -(instancetype) initWithLoadDatas
    {
        self = [super initWithLoadDatas];
        if(self!=nil){
            [self reloadDatas];
        }
        return self;
    }

    -(void) reloadDatas{
        [super reloadDatas];
        [self loadUserVerifyRest];
        [self loadUserInfoRest];
        [self loadUsersInfoRest];
        [self loadUserAddRest];
        [self loadUserDeleteRest];
        [self loadChangeAvatarRest];
        [self loadUpdateUserRest];
    }

    -(void) loadUpdateUserRest{
        RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
        [requestMapping addAttributeMappingsFromDictionary:@{
                                                             @"username":@"username",
                                                             @"password":@"password",
                                                             @"name":@"name",
                                                             @"familyId":@"familyId",
                                                             @"phone":@"phone",
                                                             @"gender":@"gender",
                                                             @"signature":@"signature"
                                                             }];
        RKRequestDescriptor *requestDescriptor =
        [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                              objectClass:[BSLUserUpdateParams class]
                                              rootKeyPath:nil
                                                   method:RKRequestMethodPOST];
        [[super requestDescriptorArr] addObject:requestDescriptor];
        //init responseDescriptor
        RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[BSLSuccessMsg class]];
        [responseMapping addAttributeMappingsFromDictionary:@{
                                                              @"success":@"isSuccess"
                                                              }];
        NSIndexSet *successCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
        RKResponseDescriptor *responseDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                     method:RKRequestMethodPOST
                                                pathPattern:@ROUTE_USER_UPDATE_PATH
                                                    keyPath:nil
                                                statusCodes:successCodes];
        [[super responseDescriptorDict] setObject:responseDescriptor forKey:@ROUTE_USER_UPDATE_ID ];
        //init route
        RKRoute *userUpdateRoute = [RKRoute routeWithRelationshipName:@ROUTE_USER_UPDATE_ID
                                                   objectClass:[BSLUserUpdateParams class]
                                                   pathPattern:@ROUTE_USER_UPDATE_PATH
                                                        method:RKRequestMethodPOST];
        [[super routeSet] addObject:userUpdateRoute];
    }

    -(void) loadChangeAvatarRest{
        RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[BSLData class]];
        [responseMapping addAttributeMappingsFromDictionary:@{
                                                              @"data":@"data"
                                                              }];
        NSIndexSet *successCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
        RKResponseDescriptor *responseDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                     method:RKRequestMethodPOST
                                                pathPattern:@ROUTE_USER_CHANGE_AVATAR_PATH
                                                    keyPath:nil
                                                statusCodes:successCodes];
        [[super responseDescriptorDict] setObject:responseDescriptor forKey:@ROUTE_USER_CHANGE_AVATAR_ID ];
        
        RKRoute *changeAvatarRoute = [RKRoute routeWithRelationshipName:@ROUTE_USER_CHANGE_AVATAR_ID
                                                      objectClass:[BSLUser class]
                                                      pathPattern:@ROUTE_USER_CHANGE_AVATAR_PATH
                                                           method:RKRequestMethodPOST];
        [[super routeSet] addObject:changeAvatarRoute];
    }

    -(void) loadUserDeleteRest{
        RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[BSLSuccessMsg class]];
        [responseMapping addAttributeMappingsFromDictionary:@{
                                                              @"success":@"isSuccess"
                                                              }];
        NSIndexSet *successCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
        RKResponseDescriptor *responseDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                     method:RKRequestMethodPOST
                                                pathPattern:@ROUTE_USER_DELETE_PATH
                                                    keyPath:nil
                                                statusCodes:successCodes];
        [[super responseDescriptorDict] setObject:responseDescriptor forKey:@ROUTE_USER_DELETE_ID ];
        
        RKRoute *deleteRoute = [RKRoute routeWithRelationshipName:@ROUTE_USER_DELETE_ID
                                                   objectClass:[BSLUser class]
                                                   pathPattern:@ROUTE_USER_DELETE_PATH
                                                        method:RKRequestMethodDELETE];
        [[super routeSet] addObject:deleteRoute];
    }

    -(void) loadUserAddRest{
        RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
        [requestMapping addAttributeMappingsFromDictionary:@{
                        @"verifyCode":@"verification_code",
                        @"username":@"username",
                        @"password":@"password",
                        @"name":@"name",
                        @"phone":@"phone",
                        @"familyName":@"family_name"
        }];
        RKRequestDescriptor *requestDescriptor =
        [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                              objectClass:[BSLUserAddParams class]
                                              rootKeyPath:nil
                                                   method:RKRequestMethodPOST];
        [[super requestDescriptorArr] addObject:requestDescriptor];
        //init responseDescriptor
        RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[BSLSuccessMsg class]];
        [responseMapping addAttributeMappingsFromDictionary:@{
                                                              @"success":@"isSuccess"
                                                              }];
        NSIndexSet *successCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
        RKResponseDescriptor *responseDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                     method:RKRequestMethodPOST
                                                pathPattern:@ROUTE_USER_ADD_PATH
                                                    keyPath:nil
                                                statusCodes:successCodes];
        [[super responseDescriptorDict] setObject:responseDescriptor forKey:@ROUTE_USER_ADD_ID ];
        //init route
        RKRoute *addRoute = [RKRoute routeWithRelationshipName:@ROUTE_USER_ADD_ID
                                                      objectClass:[BSLUserAddParams class]
                                                      pathPattern:@ROUTE_USER_ADD_PATH
                                                           method:RKRequestMethodPOST];
        [[super routeSet] addObject:addRoute];
    }

    -(void) loadUserVerifyRest{
        //init requestDescriptor
        RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
        [requestMapping addAttributeMappingsFromDictionary:@{
                @"phone":@"phone"
        }];
        RKRequestDescriptor *requestDescriptor =
            [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                                  objectClass:[BSLUser class]
                                                  rootKeyPath:nil
                                                       method:RKRequestMethodPOST];
        [[super requestDescriptorArr] addObject:requestDescriptor];
        
        //init responseDescriptor
        RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[BSLSuccessMsg class]];
        [responseMapping addAttributeMappingsFromDictionary:@{
                @"success":@"isSuccess"
        }];
        NSIndexSet *successCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
        RKResponseDescriptor *responseDescriptor =
            [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                         method:RKRequestMethodPOST
                                                    pathPattern:@ROUTE_USER_VERIFY_PATH
                                                        keyPath:nil
                                                    statusCodes:successCodes];
        [[super responseDescriptorDict] setObject:responseDescriptor forKey:@ROUTE_USER_VERIFY_ID ];
        
        //init route
//        RKRoute *verifyRoute = [RKRoute routeWithRelationshipName:@ROUTE_USER_VERIFY_NAME
//                                                      objectClass:[BSLUser class]
//                                                      pathPattern:@ROUTE_USER_VERIFY_PATH
//                                                           method:RKRequestMethodPOST];
//        [[super routeSet] addObject:verifyRoute];

    }

    -(RKResponseDescriptor *) getUserDescriptor{
        RKObjectMapping *userMapping = [RKObjectMapping mappingForClass:[BSLUser class]];
        [userMapping addAttributeMappingsFromDictionary:@{
                                                      @"id":@"id",
                                                      @"username":@"username",
                                                      @"password":@"password",
                                                      @"name":@"name",
                                                      @"familyId":@"familyId",
                                                      @"phone":@"phone",
                                                      @"gender":@"gender",
                                                      @"signature":@"signature",
                                                      @"avatarFilePath":@"avatarFilePath",
                                                      @"langCode":@"langCode",
                                                      @"registerTime":@"registerTime"
                                                      }];
        NSIndexSet *successCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
        RKResponseDescriptor *userDescriptor =
        [RKResponseDescriptor responseDescriptorWithMapping:userMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:nil
                                                keyPath:nil
                                            statusCodes:successCodes];
        return userDescriptor;
    }


    -(void) loadUserInfoRest{
        RKResponseDescriptor *userDescriptor = [self getUserDescriptor];
        [[super responseDescriptorDict] setObject:userDescriptor forKey:@ROUTE_USER_INFO_ID ];
        RKRoute *userRoute = [RKRoute routeWithRelationshipName:@ROUTE_USER_INFO_ID
                                                    objectClass:[BSLUser class]
                                                    pathPattern:@ROUTE_USER_INFO_PATH
                                                         method:RKRequestMethodGET ];

        [[super routeSet] addObject:userRoute];
    }

    -(void) loadUsersInfoRest{
        RKResponseDescriptor *userDescriptor = [self getUserDescriptor];
        [[super responseDescriptorDict] setObject:userDescriptor forKey:@ROUTE_USERS_INFO_ID ];
        RKRoute *usersRoute = [RKRoute routeWithName:@ROUTE_USERS_INFO_ID
                                         pathPattern:@ROUTE_USERS_INFO_PATH
                                              method:RKRequestMethodGET ];
        
        [[super routeSet] addObject:usersRoute];
    }


@end
