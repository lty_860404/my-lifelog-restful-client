//
//  BSLUserRouter.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSLRouteManager.h"

#define ROUTE_USER_VERIFY_ID "usersVerify"
#define ROUTE_USER_VERIFY_PATH "/users/verify"
#define ROUTE_USER_INFO_ID "userInfo"
#define ROUTE_USER_INFO_PATH "/users/:id"
#define ROUTE_USERS_INFO_ID "usersInfo"
#define ROUTE_USERS_INFO_PATH "/users"
#define ROUTE_USER_ADD_ID "userAdd"
#define ROUTE_USER_ADD_PATH "/users"
#define ROUTE_USER_DELETE_ID "userDelete"
#define ROUTE_USER_DELETE_PATH "/users/:id"
#define ROUTE_USER_CHANGE_AVATAR_ID "userChangeAvatar"
#define ROUTE_USER_CHANGE_AVATAR_PATH "/users/:id/change_avatar"
#define ROUTE_USER_UPDATE_ID "userUpdate"
#define ROUTE_USER_UPDATE_PATH "/users/:id"

@interface BSLUserRouteManager : BSLRouteManager

    

@end
